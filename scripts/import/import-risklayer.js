var loader = require('csv-load-sync');
var helper = require('../../scripts/helper.js');
var Client = require('pg-native');
var client = new Client();
client.connectSync();

loader(`dist/AnzahlFall-risklayer-by-ags.csv`).forEach(function(csvrow){
  sumSaxony = 0;
  Object.keys(helper.agsToKreisIdMapping).forEach(function(ags){
    const query = 
    `INSERT INTO public.measurement ("kreis_id","delta_yesterday","value","date","source","type","gemeinde_id") 
     VALUES (${helper.agsToKreisIdMapping[ags]}, ${csvrow[ags]}, ${csvrow[ags]}, '${csvrow.date}', 'risklayer', 'infected', 0) 
     ON CONFLICT(kreis_id, date, type, source, gemeinde_id) DO UPDATE SET value = EXCLUDED.value, delta_yesterday = EXCLUDED.delta_yesterday
     RETURNING *`;
    sumSaxony+= parseInt(csvrow[ags]);
    try {
      client.querySync(query);
    }
    catch (e) {
      console.log(`ERROR processing RKI infected file`)
      console.log(query)
      process.exit(1);
    }
  })
  // combine alle values for each day and ags to saxony value
  try {
    client.querySync(`
    INSERT INTO public.measurement ("kreis_id","delta_yesterday","value","date","source","type","gemeinde_id") 
    VALUES (14, ${sumSaxony}, ${sumSaxony}, '${csvrow.date}', 'risklayer', 'infected', 0) 
    ON CONFLICT(kreis_id, date, type, source, gemeinde_id) DO UPDATE SET value = EXCLUDED.value, delta_yesterday = EXCLUDED.delta_yesterday
    RETURNING *`);
  }
  catch (e) {
    console.log(`ERROR processing RKI infected file`)
    console.log(query)
    process.exit(1);
  }
})
