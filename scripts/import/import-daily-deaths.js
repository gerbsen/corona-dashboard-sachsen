const moment = require('moment');
moment.locale("de_DE");
const fs = require('fs');
const parse = require('csv-parse/lib/sync');
const commons = require('../replacements/commons');
const getJSON = require('get-json');
const Client = require('pg-native');
const client = new Client();
client.connectSync();
const arguments = process.argv.slice(2);

async function importData(file, dateformat = "DD.MM.YYYY"){
  
  // Read the content
  const content = await fs.promises.readFile(file)
  // Parse the CSV content
  const records = parse(content, {columns : true}).filter(item => item.IdBundesland == '14');

  // data comes in earliest at 00:00 for the past day
  const date = moment(records[1].Datenstand.replace(/\//g, ".").replace(", 00:00 Uhr", ""), dateformat).subtract(1, 'days');
  console.log(`Importing daily cases for ${date.format("DD.MM.YYYY")}`)

  let sumDeaths = 0, sumCases = 0;
  for (const [ags, id] of Object.entries(commons.agsToKreisIdMapping)) {
        
    const deathToday = records.filter(item => (item.NeuerTodesfall == 1 || item.NeuerTodesfall == -1) && item.IdLandkreis == ags)
      .reduce((accumulator, item) => accumulator + parseInt(item.AnzahlTodesfall), 0);

    const casesToday = records.filter(item => (item.NeuerFall == 1 || item.NeuerFall == -1) && item.IdLandkreis == ags)
      .reduce((accumulator, item) => accumulator + parseInt(item.AnzahlFall), 0);

    sumDeaths += deathToday;
    sumCases += casesToday;

    client.querySync(`
      INSERT INTO "measurement"("kreis_id","date","source","value","type","gemeinde_id")
      VALUES (${id},'${date.format("YYYY-MM-DD")}'::date,'rki',${deathToday},'death-today', 0)
      ON CONFLICT(kreis_id, date, type, source, gemeinde_id) DO UPDATE SET value = EXCLUDED.value, delta_yesterday = EXCLUDED.delta_yesterday`);
    client.querySync(`
      INSERT INTO "measurement"("kreis_id","date","source","value","type","gemeinde_id")
      VALUES (${id},'${date.format("YYYY-MM-DD")}'::date,'rki',${casesToday},'cases-today', 0)
      ON CONFLICT(kreis_id, date, type, source, gemeinde_id) DO UPDATE SET value = EXCLUDED.value, delta_yesterday = EXCLUDED.delta_yesterday`);
  }

  client.querySync(`
    INSERT INTO "measurement"("kreis_id","date","source","value","type","gemeinde_id")
    VALUES (14,'${date.format("YYYY-MM-DD")}'::date,'rki',${sumDeaths},'death-today', 0)
    ON CONFLICT(kreis_id, date, type, source, gemeinde_id) DO UPDATE SET value = EXCLUDED.value, delta_yesterday = EXCLUDED.delta_yesterday`);
  client.querySync(`
    INSERT INTO "measurement"("kreis_id","date","source","value","type","gemeinde_id")
    VALUES (14,'${date.format("YYYY-MM-DD")}'::date,'rki',${sumCases},'cases-today', 0)
    ON CONFLICT(kreis_id, date, type, source, gemeinde_id) DO UPDATE SET value = EXCLUDED.value, delta_yesterday = EXCLUDED.delta_yesterday`);
}

const file = arguments[0] || `dist/RKI_COVID19.csv`;
const dateformat = arguments[1] || "DD.MM.YYYY";
importData(file, dateformat);