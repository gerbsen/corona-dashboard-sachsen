const moment = require('moment');
moment.locale("de_DE");
const fs = require('fs');
const parse = require('csv-parse/lib/sync');
const commons = require('../replacements/commons');
const getJSON = require('get-json');
const Client = require('pg-native');
const client = new Client();
client.connectSync();
const arguments = process.argv.slice(2);

const sample = {
	"last_updated": "2021-04-04T07:23:31Z",
	"population": 4071971,
	"r_t_threshold_probability": 0.78375,
	"r_t": 1.169061774992294,
	"r_t_lower": 0.7856588676371976,
	"r_t_upper": 1.49671657399532,
	"infections_by_100k": 138.57520017501872,
	"infections_by_100k_lower": 37.23627137531084,
	"infections_by_100k_upper": 271.57255543395524,
	"infections_absolute": 5642.741964318711,
	"infections_absolute_lower": 1516.2501718839587,
	"infections_absolute_upper": 11058.355701229582,
	"total": 603982.8670099102,
	"total_lower": 570932.0023618626,
	"total_upper": 646376.061719998,
	"active": 26911.23804724207,
	"active_lower": 10926.251061185378,
	"active_upper": 49639.953122679835
}

getJSON('https://rtlive.de/data/de_SN_summary.json', function(error, response){
 
  Object.keys(sample).forEach(key => {

    console.log(key)

    const date = moment(response.date);
    response.last_updated = moment(response.last_updated).valueOf();
    const value = response[key];

    client.querySync(`
      INSERT INTO "measurement"("kreis_id","date","source","value","type","gemeinde_id")
      VALUES (14,'${date.format("YYYY-MM-DD")}'::date,'rtlive',${value},'${key}',0)
      ON CONFLICT(kreis_id, date, type, source, gemeinde_id) DO UPDATE SET value = EXCLUDED.value, delta_yesterday = EXCLUDED.delta_yesterday`);
  })
});