const moment = require('moment');
moment.locale("de_DE");
const parse = require('csv-parse/lib/sync');
const commons = require('../replacements/commons');
const getJSON = require('get-json');
var sql = require('../../scripts/sql.js');

getJSON('https://www.coronavirus.sachsen.de/corona-statistics/rest/incidenceVaccinationDevelopment.jsp', function(error, response){
 
	['incidenceNotVaccinated', 'incidenceFullyVaccinated'].forEach(function(type){

		var date = moment(response[type].startDate);
		for (var index = 0; index < response[type].values.length; index++, date.add(1, 'day')) {
			sql.upsertKreisValue(14, "7d-" + type, response[type].values[index], date.format("YYYY-MM-DD"), 'sms');
		};
	})  
});