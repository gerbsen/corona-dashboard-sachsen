// https://www.arcgis.com/sharing/rest/content/items/f10774f1c63e40168479a1feb6c7ca74/data 
const replace = require('replace-in-file');
let fs = require("fs");
let path = require("path");
const _ = require('lodash');
const moment = require('moment');
var sql = require('../../scripts/sql.js');  

async function createCsv(){

  const infections = sql.getInfectionsSumForLastXDays(5, 50);
  console.log("city,date,diagnostiziert")
  for (const [date, value] of Object.entries(infections)) {
    console.log(`Erzgebirge,${date},${value}`)
  }
}

createCsv();