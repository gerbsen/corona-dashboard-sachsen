#!/bin/bash

# save the postgresql connection settings in one variable
POSTGRES="PGUSER=${PGUSER:=corona} PGHOST=${PGHOST:=localhost} PGPASSWORD=${PGPASSWORD} PGDATABASE=${PGDATABASE:=corona} PGPORT=${PGPORT:=5432}"

# copy a dist file from the template
cp html/impfstatus.template.html dist/impfstatus.html
mkdir -p dist/json/ 
mkdir -p dist/r/
mkdir -p dist/images/
mkdir -p dist/covidsim/
cp -f html/json/sachsen.js dist/json/sachsen.js
cp -f html/covidsim/cosim-Sachsen-current.csv dist/covidsim/cosim-Sachsen-current.csv
cp -f html/images/impfbereitschaft_deutschland.png dist/images/impfbereitschaft_deutschland.png

printf "%-05s%-35s%-20s\n" "###" "Generator" "Time in seconds"
printf "%-05s%-35s%-20s\n" "---" "--------------------------------" "---------"
TOTALSTARTTIME=$(date +%s)

# run all the replacements one after the other
STARTTIME=$(date +%s)
env $POSTGRES node scripts/replacements/032_impfstatus.js "$@"
ENDTIME=$(date +%s)
printf "%-05s%-35s%-20s\n" "032" "impfstatus" "$(($ENDTIME - $STARTTIME))s"