#!/bin/bash
set -e
# clean output directory
CLEAN="${CLEAN:=false}"
if [ "$CLEAN" = true ] ; then
  rm -rf dist/*
fi

currentDir="$(cd "$(dirname "$1")"; pwd -P)/$(basename "$1")"
POSTGRES="PGUSER=${PGUSER:=corona} PGHOST=${PGHOST:=localhost} PGPASSWORD=${PGPASSWORD} PGDATABASE=${PGDATABASE:=corona} PGPORT=${PGPORT:=5432}"

# walk over all files from the SMS and DIVI and import them
echo "1/2 - Importing infection and death data from SMS and hospital data from DIVI"
./scripts/run-import-files.sh "$@"

# call the other script to not have things duplicated
echo "2/2 - Generating output HTML file"
./scripts/run-generator.sh "$@"