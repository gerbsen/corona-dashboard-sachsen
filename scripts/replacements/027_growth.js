const replace = require('replace-in-file');
const fs = require("fs");
const path = require("path");
const moment = require('moment');
const _ = require('lodash');
const sql = require('../../scripts/sql.js');
const helper = require('../../scripts/helper.js');
const commons = require('./commons');


const numberOfDays = commons.days;

commons.kreise.forEach(kreis_id => {    

  const results = {
    positiveValues : [],
    negativeValues : [],
    labels : [],
  };
  
  const today = moment(sql.getMaxDate());
  
  for (let index = 0; index < numberOfDays; index++) {

    const last7DayInfections = sql.getInfectionBetweenDates(kreis_id, 
      moment(today).subtract(index, 'days'), moment(today).subtract(7 + index, 'days'));

    const last7DayInfectionsLastWeek = sql.getInfectionBetweenDates(kreis_id, 
      moment(today).subtract(index + 7, 'days'), moment(today).subtract(7 + index + 7, 'days'));

    // more informatione here: https://twitter.com/f2135/status/1375364986378276865?s=20
    const growthValue = _.round((Math.log2(last7DayInfections/last7DayInfectionsLastWeek)/Math.log2(2)/7 * 100), 1);

    results.positiveValues.push(growthValue >= 0 ? growthValue : 0);
    results.negativeValues.push(growthValue <= 0 ? growthValue : 0);
    results.labels.push("\"" + moment(today).subtract(index, 'days').format("DD.MM.") + "\"")
  }

  var growthTemplate = fs.readFileSync(path.join(__dirname, "../../html/growth_template.js"), "utf8");
  growthTemplate += "\n/* JAVASCRIPT_NEEDLE */"
  growthTemplate = growthTemplate.replace(/GROWTH_KREIS_ID/g, kreis_id);
  growthTemplate = growthTemplate.replace(/GROWTH_LABELS/g,   results.labels.reverse().toString());
  growthTemplate = growthTemplate.replace('IMPORTANT_DATES', `,${JSON.stringify(commons.importantDates).replace(/^\[/g, "").replace(/\]$/g, "")}`);

  growthTemplate = growthTemplate.replace(/GROWTH_DATASETS/g, `{
    label: 'Wachstum',
    backgroundColor: '#d7191c',
    borderColor: '#d7191c',
    fill: 'origin',
    data: [${results.positiveValues.reverse().toString()}],
  },{
    label: 'Reduktion',
    backgroundColor: '#1a9641',
    borderColor: '#1a9641',
    fill: 'origin',
    data: [${results.negativeValues.reverse().toString()}],
  },`);

  var onloadHtml = `
    var ctx = document.getElementById('growth-canvas-${kreis_id}').getContext('2d');
    window.canvas${kreis_id} = new Chart(ctx, growth${kreis_id}ChartConfig);
    /* WINDOW_ONLOAD_NEEDLE */`
  replace.sync({ files: 'dist/index_in_progress.html', from: `/* WINDOW_ONLOAD_NEEDLE */`, to: onloadHtml });
  replace.sync({ files: 'dist/index_in_progress.html', from: `/* JAVASCRIPT_NEEDLE */`, to: growthTemplate });
});

