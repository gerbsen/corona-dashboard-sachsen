const replace = require('replace-in-file');
const fs = require("fs");
const path = require("path");
const moment = require('moment');
const sma = require('sma');
const sql = require('../../scripts/sql.js');
const commons = require('./commons');

commons.kreise.forEach(kreise_id => {

  const days           = commons.days;
  const infectedValues = sql.getInfectionsForDays(kreise_id, days);
  const deathValues    = sql.getInfectionsForDays(kreise_id, days, 'death-today');
  var smoothed         = sma(Object.values(sql.getInfectionsForDays(kreise_id, days + 6)), 7);

  var infectionsTemplate = fs.readFileSync(path.join(__dirname, "../../html/infections_template.js"), "utf8");
  infectionsTemplate = infectionsTemplate.replace(/DIFF_YESTERDAY_ID/g, kreise_id);
  infectionsTemplate = infectionsTemplate.replace('DIFF_YESTERDAY_LABELS', Object.keys(infectedValues).map(date => `"${moment(date).format("DD.MM.")}"`).join());
  infectionsTemplate = infectionsTemplate.replace('BACKGROUND_COLORS', Object.keys(infectedValues).map(date => "window.chartColors.red").join());
  infectionsTemplate = infectionsTemplate.replace('DIFF_YESTERDAY_VALUES',  Object.values(infectedValues).join());
  infectionsTemplate = infectionsTemplate.replace('DIFF_YESTERDAY_DEAD_VALUES', Object.values(deathValues).join());
  infectionsTemplate = infectionsTemplate.replace('DIFF_YESTERDAY_SMOOTHED_VALUES',  smoothed.join());
  infectionsTemplate = infectionsTemplate.replace('IMPORTANT_DATES', `${JSON.stringify(commons.importantDates).replace(/^\[/g, "").replace(/\]$/g, "")},`);
  infectionsTemplate += "\n/* JAVASCRIPT_NEEDLE */"

  const hospitalized = []
  if ( kreise_id == 14 ) {
    Object.entries(infectedValues).forEach(infectedValue => {
      // console.log(infectedValue)
      const result = sql.getHospitalizedForDate(moment(infectedValue[0]));
      hospitalized.push(result.numberOfOccupiedBeds ? result.numberOfCovidOnNormalStation : undefined);
    })
  }  

  infectionsTemplate = infectionsTemplate.replace('Y_AXIS_ID', 'y-axis-1');
  infectionsTemplate = infectionsTemplate.replace('SMS_HOSPITAL_VALUES', `,{
      label: 'Belegung der Betten (SMS, Daten nur Werktags)',
      backgroundColor: window.chartColors.yellow,
      borderColor: window.chartColors.yellow,
      data: [${hospitalized.join()}],
      fill: false,
      type: 'line',
      order: 0,
      yAxisID: "y-axis-2",
      spanGaps: true
    } 
  `);
  infectionsTemplate = infectionsTemplate.replace('NOTBREMSE', kreise_id == 14 ? 
    `
    {
      drawTime: "afterDatasetsDraw",
      id: "hline1",
      type: "line",
      mode: "horizontal",
      scaleID: "y-axis-2",
      value: 1300,
      borderColor: "black",
      borderWidth: 1,
      label: {
        backgroundColor: "#d7191c",
        content: "Überlastungsstufe: 1300 Betten",
        enabled: true
      }
    },
    {
      drawTime: "afterDatasetsDraw",
      id: "hline2",
      type: "line",
      mode: "horizontal",
      scaleID: "y-axis-2",
      value: 650,
      borderColor: "black",
      borderWidth: 1,
      label: {
        backgroundColor: "#d7191c",
        content: "Vorwarnstufe: 650 Betten",
        enabled: true
      }
    }
  ` : '')
  infectionsTemplate = infectionsTemplate.replace('DISPLAY_SECOND_Y_AXES', false);
  infectionsTemplate = infectionsTemplate.replace(/VIEW_HORIZONTAL_LINE_LABEL/g, true);
  infectionsTemplate = infectionsTemplate.replace(/VIEW_HORIZONTAL_LINE/g, 'true');
  
  var onloadHtml = `
      var ctx = document.getElementById('diff-yesterday-canvas-${kreise_id}').getContext('2d');
      window.canvas${kreise_id} = new Chart(ctx, diffYesterdayCanvas${kreise_id}Config);
      /* WINDOW_ONLOAD_NEEDLE */`
  replace.sync({ files: 'dist/index_in_progress.html', from: `/* WINDOW_ONLOAD_NEEDLE */`, to: onloadHtml });
  replace.sync({ files: 'dist/index_in_progress.html', from: `/* JAVASCRIPT_NEEDLE */`, to: infectionsTemplate });
});