const replace = require('replace-in-file');
const fs = require("fs");
const path = require("path");
const moment = require('moment');
const _ = require('lodash');
const parse = require('csv-parse/lib/sync');
const commons = require('./commons');
const { constants } = require('buffer');

// Datum,Bundesland,Bundesland_Id,Altersgruppe,7T_Hospitalisierung_Faelle,7T_Hospitalisierung_Inzidenz
// 2021-11-20,Sachsen,14,00-04,6,3.33
// 2021-11-20,Sachsen,14,00+,137,3.38
// 2021-11-20,Sachsen,14,05-14,3,0.82
// 2021-11-20,Sachsen,14,15-34,9,1.14
// 2021-11-20,Sachsen,14,35-59,15,1.11
// 2021-11-20,Sachsen,14,60-79,48,4.75
// 2021-11-20,Sachsen,14,80+,56,15.47
async function run(){
  
  const content = await fs.promises.readFile(`dist/Aktuell_Deutschland_COVID-19-Hospitalisierungen.Sachsen.csv`)
  // Parse the CSV content
  const records = parse(content, {columns : true});
  const saxony = { '00-04' : [], '00+'   : [], '05-14' : [],
                   '15-34' : [], '15-34' : [], '35-59' : [],
                   '60-79' : [], '80+'   : []};
  
  const dates = [...new Set(records.map(record => record.Datum))];

  records.forEach(function(record){
    Object.keys(saxony).forEach(function(ageGroup){
      saxony[record.Altersgruppe].push({ 
        x: moment(record.Datum).format("DD.MM.YYYY"),
        y : record['7T_Hospitalisierung_Inzidenz']
      });
    });
  });

  replace.sync({ files: 'dist/index_in_progress.html', from: `/* WINDOW_ONLOAD_NEEDLE */`, to: getChart(saxony, dates) });
}

function getChart(values, dates){

  const dateFormat = "DD.MM.YYYY";
  const sortedAndFormatedDates = _.map(dates, date => new Date(date))
                                  .sort((date1, date2) => date1 - date2)
                                  .map(b => moment(b).format(dateFormat));

  return `
    var ctx = document.getElementById('hospitalization-incidence-canvas-14').getContext('2d');
    window.hospitalizationIncidenceCanvas14Chart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: ${JSON.stringify(sortedAndFormatedDates)},
        datasets: [
          {
            label: 'Gesamt',
            data: ${JSON.stringify(values['00+'])},
            backgroundColor: window.chartColors.red,
            borderColor: window.chartColors.red,
            fill: false
          },
          {
            label: '0-4 Jahre',
            data: ${JSON.stringify(values['00-04'])},
            backgroundColor: window.chartColors.orange,
            borderColor: window.chartColors.orange,
            fill: false
          },
          {
            label: '05-14 Jahre',
            data: ${JSON.stringify(values['05-14'])},
            backgroundColor: window.chartColors.yellow,
            borderColor: window.chartColors.yellow,
            fill: false
          },
          {
            label: '15-34 Jahre',
            data: ${JSON.stringify(values['15-34'])},
            backgroundColor: window.chartColors.green,
            borderColor: window.chartColors.green,
            fill: false
          },
          {
            label: '35-59 Jahre',
            data: ${JSON.stringify(values['35-59'])},
            backgroundColor: window.chartColors.blue,
            borderColor: window.chartColors.blue,
            fill: false
          },
          {
            label: '60-79 Jahre',
            data: ${JSON.stringify(values['60-79'])},
            backgroundColor: window.chartColors.purple,
            borderColor: window.chartColors.purple,
            fill: false
          },
          {
            label: '80+ Jahre ',
            data: ${JSON.stringify(values['80+'])},
            backgroundColor: window.chartColors.black,
            borderColor: window.chartColors.black,
            fill: false
          }
      ]
      },
      options: {
        responsive: true,
        interaction: {
          intersect: false,
          mode: 'index',
        },
        legend: { position: 'bottom' },
        elements: {
          point:{
            radius: 0
          }
        },
        scales: {
          x: {
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'Datum'
            }
          },
          y: {
            display: true,
            position: 'left',
            scaleLabel: {
              display: true,
              labelString: 'Hospitalisierungsinzidenz nach Altersgruppen'
            },
            gridLines: {
              display: true,
              drawBorder: true,
              drawOnChartArea: true,
              drawTicks: true,
            }
          }
        }
      }
    });
    
    /* WINDOW_ONLOAD_NEEDLE */`
}

run();
