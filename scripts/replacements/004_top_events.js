const replace = require('replace-in-file');
const moment = require('moment');
const sma = require('sma');
const _ = require('lodash');
const commons = require('./commons.js');
moment.locale("de_DE");
var sql = require('../../scripts/sql.js');
const helper = require('../../scripts/helper.js');
const numberOfDays = commons.days;

const buildGraphs = function() {

  var results = { 'values' : ''}; 
  var maxDate = sql.getMaxDate();
  commons.kreise.forEach(function(kreis_id){  

    results[kreis_id] = {};
    results[kreis_id].values = [];
    results[kreis_id].realValues = [];
    results[kreis_id].labels = [];
    
    _.range(numberOfDays).forEach(function(days){
      results[kreis_id].values.push(sql.get7dIncidenceForKreisToday(kreis_id, days));
      results[kreis_id].realValues.push(sql.getXDaysIncidenceForDistrict(kreis_id, 7 + days, 2, days));
      results[kreis_id].labels.push("\"" + moment(maxDate).subtract(days, 'days').format("DD.MM.") + "\"")
    })

    // var smoothed  = sma(results[kreis_id].values.reverse(), 7);
    smoothed = results[kreis_id].values.reverse();
    
    results.values += `{
      label: '${helper.getName(kreis_id)}',
      backgroundColor: '${helper.getColor(kreis_id)}',
      borderColor: '${helper.getColor(kreis_id)}',
      fill: false,
      data: [${smoothed.toString()}],
    },`;
    // Nachmeldungen erstmal deaktiviert, da Daten nicht wirklich erfasst werden
    // ,{
    //   label: 'Nachmeldeinzidenz: ${helper.getName(kreis_id)}',
    //   backgroundColor: '${helper.getLighterColor(kreis_id)}',
    //   borderColor: '${helper.getColor(kreis_id)}',
    //   fill: false,
    //   data: [${results[kreis_id].realValues.reverse().toString()}],
    // },`
  })

  // let's add the unsmoothed values for comparision to other websites
  // results.values += `{
  //   label: 'Sachsen (keine Glättung)',
  //   backgroundColor: '${helper.getColor(14)}',
  //   borderColor: '${helper.getColor(14)}',
  //   fill: false,
  //   data: [${results[14].values.toString()}],
  // },`

  var dates = results[12].labels.reverse();//.slice(7, results[12].length)
  // var dates = results[12].labels.reverse().slice(7, results[12].length)

  replace.sync({ files: 'dist/index_in_progress.html', from: `SEVEN_DAYS_CAPITA_DATASETS`, to: results.values });
  replace.sync({ files: 'dist/index_in_progress.html', from: `SEVEN_DAYS_CAPITA_LABELS`, to: dates.toString() });
}

const buildTable = function(){
  
  const today = moment(sql.getMaxDate());
  var headerNeuinfektion = [];
  for (var i = 0; i < 7; i++)
    headerNeuinfektion.push(`<th>${moment(today).subtract(i, 'days').format("D.M.")}</td>`)

  var table = `
    <table class="table table-sm table-striped" id="top_table">
      <thead>
        <tr>
          <th scope="col">Landkreis/ kreisfreie Stadt</th>
          ${headerNeuinfektion.reverse().join("")}
          <th scope="col">7d-Summe</th>
          <th scope="col">7d-Inzidenz</th>
          <th scope="col">Vgl. gestern</th>
          <th scope="col">Vgl. Vorwoche</th>
        </tr>
      </thead>
      <tbody>`;

  commons.kreise.forEach(function(kreis_id){  

    var theDate = moment(sql.getMaxDate());
    var tdInfections = [];
    var summe = 0;
    for (var i = 0; i < 7; i++) {
      const infections = sql.getInfectionsForDistrictAndDate(kreis_id, moment(theDate).subtract(i, 'days').format("YYYY-MM-DD"));
      tdInfections.push(`<td>${infections}</td>`)
      summe += infections;
    }
    tdInfections = tdInfections.reverse().join("")
    tdInfections += `<td>${summe}</td>`

    const lastWeekIncidence  = sql.get7dIncidenceForKreisOneWeekBefore(kreis_id);
    const yesterdayIncidence = sql.get7dIncidenceForKreisYesterday(kreis_id); 
    const todayIncidence     = sql.get7dIncidenceForKreisToday(kreis_id);
    const yesterdayDiff      = todayIncidence - yesterdayIncidence;
    const lastweekDiff       = todayIncidence - lastWeekIncidence;

    const tds =
      `<td><div style="background: ${commons.colors[kreis_id]};width: 2vw; height: 1vw;"></div>${commons.names[kreis_id]}</td>
       ${tdInfections}
       <td>${sql.getXDaysIncidenceForDistrict(kreis_id, 7)}</td>
       <td>${ (yesterdayDiff < 0 ? "" : "+" ) + yesterdayDiff.toFixed(1) } ${helper.getIncidenceTrendIcon(todayIncidence, yesterdayIncidence)}</td>
       <td>${ (lastweekDiff < 0 ? "" : "+" ) + lastweekDiff.toFixed(1) } ${helper.getIncidenceTrendIcon(todayIncidence, lastWeekIncidence)}</td>
       `

    if (kreis_id == 14) {
      table += `
        <tfoot>
          <tr style="font-weight: bold;">
            ${tds}
          </tr>
        </tfoot>
      `
    }
    else 
      table += `<tr>${tds}</tr>`
  })
  table += `</tbody></table>`;

  replace.sync({ files: 'dist/index_in_progress.html', from: `TABLE_infected`, to: table });

  var date = sql.getMaxDate();
  replace.sync({ files: 'dist/index_in_progress.html', from: /TABLE_DATE/g, to: `${moment(date).add(1, 'days').format("DD.MM.YYYY")}` });
}

buildGraphs();
buildTable();