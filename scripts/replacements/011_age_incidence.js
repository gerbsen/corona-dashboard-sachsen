// https://www.arcgis.com/sharing/rest/content/items/f10774f1c63e40168479a1feb6c7ca74/data 
const replace = require('replace-in-file');
const parse = require('csv-parse/lib/sync');
const fs = require('fs');
const _ = require('lodash');
const moment = require('moment');
var helper = require('../../scripts/helper.js');
let path = require("path");
const commons = require('./commons');

function getCount(rows, gender, years, currentDate){

  const infectionsPerAgePerGender = rows.filter(item => 
      item.Geschlecht.match(gender) && 
      item.Altersgruppe == years && 
      moment(item.Meldedatum.replace(" 00:00:00", ""), "YYYY/MM/DD").isBetween(moment(currentDate).subtract(6, 'days'), currentDate, undefined, '[]')
    )
    .reduce((accumulator, landkreisValue) => accumulator + parseInt(landkreisValue.AnzahlFall), 0)

  return infectionsPerAgePerGender;
}

function fillTemplate(kreis_id, data){
  
  var infectionsTemplate = fs.readFileSync(path.join(__dirname, "../../html/age_incidence_template.js"), "utf8");
  infectionsTemplate = infectionsTemplate.replace(/KREIS_ID/g, kreis_id);
  infectionsTemplate = infectionsTemplate.replace(/CONFIG_NAME/g, `ageIncidence${kreis_id}Config`);
  infectionsTemplate = infectionsTemplate.replace(/GRAPH_TITLE/g, 'Inzidenzen nach Altersklassen (Quelle: RKI)');
  infectionsTemplate = infectionsTemplate.replace(/X_AXIS_LABEL/g, 'Datum');
  infectionsTemplate = infectionsTemplate.replace(/Y_AXIS_LABEL/g, 'Fälle pro 100.000 Einwohner pro 7 Tage');
  infectionsTemplate = infectionsTemplate.replace(/AGE_INCIDENCE_DATES/g,   data['AGE_INCIDENCE_DATES']);
  infectionsTemplate = infectionsTemplate.replace(/AGE_INCIDENCE_0_4_VALUES/g,   data['M|W-A00-A04']);
  infectionsTemplate = infectionsTemplate.replace(/AGE_INCIDENCE_5_14_VALUES/g,  data['M|W-A05-A14']);
  infectionsTemplate = infectionsTemplate.replace(/AGE_INCIDENCE_15_34_VALUES/g, data['M|W-A15-A34']);
  infectionsTemplate = infectionsTemplate.replace(/AGE_INCIDENCE_35_59_VALUES/g, data['M|W-A35-A59']);
  infectionsTemplate = infectionsTemplate.replace(/AGE_INCIDENCE_60_79_VALUES/g, data['M|W-A60-A79']);
  infectionsTemplate = infectionsTemplate.replace(/AGE_INCIDENCE_80_VALUES/g,    data['M|W-A80+']);
  infectionsTemplate += "\n/* JAVASCRIPT_NEEDLE */"

  var onloadHtml = `
    var ctx = document.getElementById('age-incidence-canvas-${kreis_id}').getContext('2d');
    window.canvas_age_incidence_${kreis_id} = new Chart(ctx, ageIncidence${kreis_id}Config);
    /* WINDOW_ONLOAD_NEEDLE */`;

  replace.sync({ files: 'dist/index_in_progress.html', from: `/* WINDOW_ONLOAD_NEEDLE */`, to: onloadHtml });
  replace.sync({ files: 'dist/index_in_progress.html', from: `/* JAVASCRIPT_NEEDLE */`, to: infectionsTemplate });
}

async function statisticalOverview(){
  
  // Read the content
  const content = await fs.promises.readFile(`dist/RKI_COVID19.csv`)
  // Parse the CSV content
  const records = parse(content, {columns : true})
  const fromDate = moment().subtract(commons.days, 'days')
  const toDate   = moment();
  const sachsen = records.filter(item => item.IdBundesland == '14' && toDate.diff(moment(item.Meldedatum.replace(" 00:00:00", ""), "YYYY/MM/DD"), 'days') <= commons.days);

  const genderGroups = ['M', 'W', 'M|W'];
  const ageGroups = ['A00-A04', 'A05-A14', 'A15-A34', 'A35-A59', 'A60-A79', 'A80+'];
  const dates    = [];  
  for (var currentDate = moment(fromDate); currentDate.diff(toDate, 'days') < 0; currentDate.add(1, 'days')) {
    dates.push(`"${currentDate.format("D.MM.")}"`)
  }

  // for all the kreise and kreisfreie staedte
  for (const [key, value] of Object.entries(helper.agsToKreisIdMapping)) {
    
    const landkreisValues = sachsen.filter(item => item.IdLandkreis == key);  
    const data = { AGE_INCIDENCE_DATES : dates}
    
    genderGroups.forEach(gender => {
      ageGroups.forEach(years => {

        const genderAgeValue = [];

        for (var currentDate = moment(fromDate); currentDate.diff(toDate, 'days') < 0; currentDate.add(1, 'days')) {
          const infectionsPerAgePerGender = getCount(landkreisValues, gender, years, currentDate)
          genderAgeValue.push(Math.round(((infectionsPerAgePerGender / helper.getPopulationByAgs(key, years)) * 100000) * 10) / 10)        
        }

        data[`${gender}-${years}`] = genderAgeValue
      })
    })

    fillTemplate(helper.agsToKreisIdMapping[key], data);
  }

  const data = { AGE_INCIDENCE_DATES : dates}
  // all of saxony
  genderGroups.forEach(gender => {
    ageGroups.forEach(years => {

      const genderAgeValue = [];

      for (var currentDate = moment(fromDate); currentDate.diff(toDate, 'days') < 0; currentDate.add(1, 'days')) {
      
        const infectionsPerAgePerGender = getCount(sachsen, gender, years, currentDate)
        genderAgeValue.push(Math.round(((infectionsPerAgePerGender / helper.getPopulationByAgs('14', years)) * 100000) * 10) / 10)        
      }
      data[`${gender}-${years}`] = genderAgeValue
    })
  })
  fillTemplate(14, data);
}

statisticalOverview();