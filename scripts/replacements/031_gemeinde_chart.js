const replace = require('replace-in-file');
const fs = require("fs");
const path = require("path");
const moment = require('moment');
const _ = require('lodash');
const sql = require('../../scripts/sql.js');
const helper = require('../../scripts/helper.js');
const commons = require('./commons');
const csv = require('../../scripts/csv.js');

// labels: [${Array.from(labels).join()}],
//         datasets: [{
//           label: 'Inzidenz für Sachsen',
//           backgroundColor: window.chartColors.red,
//           borderColor: window.chartColors.red,
//           data: ${JSON.stringify(data.INCIDENCE)},
//           fill: false,
//           hidden: false
//         }]

commons.kreise.filter(k => k > 3 && k != 14).forEach(kreis_id => {

  var onloadHtml = `
    var ctx = document.getElementById('gemeindeChart${kreis_id}Canvas').getContext('2d');
    window.gemeindeChart${kreis_id}CanvasChart = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: [],
        datasets: []
      },
      options: {
        responsive: true,
        title: { display: false },
        tooltips: { mode: 'index', intersect: false },
        hover: { mode: 'nearest', intersect: true },
        legend: { position: 'bottom' },
        scales: {
          xAxis: {
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'Datum'
            }
          },
          'y-axis-1': {
            display: true,
            position: 'left',
            id: 'y-axis-1',
            scaleLabel: {
              display: true,
              labelString: '7-Tages-Neuinfektionen'
            },
            gridLines: {
              display: true,
              drawBorder: true,
              drawOnChartArea: true,
              drawTicks: true,
            }
          },
          'y-axis-2' : {
            display: true,
            position: 'right',
            id: 'y-axis-2',
            scaleLabel: {
              display: true,
              labelString: '7-Tages-Inzidenz'
            },
            gridLines: {
              display: true,
              drawBorder: true,
              drawOnChartArea: true,
              drawTicks: true,
            }
          }
        }
      }
    });
    /* WINDOW_ONLOAD_NEEDLE */`
  replace.sync({ files: 'dist/index_in_progress.html', from: `/* WINDOW_ONLOAD_NEEDLE */`, to: onloadHtml });
})
