// https://www.arcgis.com/sharing/rest/content/items/f10774f1c63e40168479a1feb6c7ca74/data 
const replace = require('replace-in-file');
let fs = require("fs");
let path = require("path");
const _ = require('lodash');
const moment = require('moment');
var csv = require('../../scripts/csv.js');0
const commons = require('./commons');
var sql = require('../../scripts/sql.js');

let gemeinden = JSON.parse(fs.readFileSync('html/json/sachsen_gemeinden.geojson'));
let results = {
  good : { count: 0, population : 0 },
  okay : { count: 0, population : 0 },
  soso : { count: 0, population : 0 },
  same : { count: 0, population : 0 },
  worse : { count: 0, population : 0 },
  bad : { count: 0, population : 0 },
  group1 : { count: 0, population : 0 },
  group2 : { count: 0, population : 0 },
  group3 : { count: 0, population : 0 },
  group4 : { count: 0, population : 0 },
  group5 : { count: 0, population : 0 },
  group6 : { count: 0, population : 0 },
  group7 : { count: 0, population : 0 },
  group8 : { count: 0, population : 0 },
  group9 : { count: 0, population : 0 },
  group10 : { count: 0, population : 0 }
}

var start = "", end = "";

gemeinden.features.forEach(gemeinde => {
  const infections = sql.getValueForGemeindeById(gemeinde.properties.id, "7d-infections")[0];
  const incidence = sql.getValueForGemeindeById(gemeinde.properties.id, "7d-incidence")[0];

  start = incidence.date;
  end = sql.getValueForGemeindeById(gemeinde.properties.id, "7d-incidence", "sms", 1, 7)[0].date;
  
  gemeinde.properties['7dIncidence7dDiff'] = _.round(sql.getValueForGemeindeById(gemeinde.properties.id, "7d-incidence")[0]['value'] -
    sql.getValueForGemeindeById(gemeinde.properties.id, "7d-incidence", "sms", 1, 7)[0]['value'], 1);

  if ( gemeinde.properties['7dIncidence7dDiff'] <= -75 ) {
    results.good.count += 1;
    results.good.population += gemeinde.properties.einwohner;
  }
  else if ( gemeinde.properties['7dIncidence7dDiff'] > -75 && gemeinde.properties['7dIncidence7dDiff'] <= -25) {
    results.okay.count += 1;
    results.okay.population += gemeinde.properties.einwohner;
  }
  else if ( gemeinde.properties['7dIncidence7dDiff'] > -25 && gemeinde.properties['7dIncidence7dDiff'] <= 0) {
    results.soso.count += 1;
    results.soso.population += gemeinde.properties.einwohner;
  }
  else if ( gemeinde.properties['7dIncidence7dDiff'] > 0 && gemeinde.properties['7dIncidence7dDiff'] <= 25) {
    results.same.count += 1;
    results.same.population += gemeinde.properties.einwohner;
  }
  else if ( gemeinde.properties['7dIncidence7dDiff'] > 25 && gemeinde.properties['7dIncidence7dDiff'] <= 75) {
    results.worse.count += 1;
    results.worse.population += gemeinde.properties.einwohner;
  }
  else if ( gemeinde.properties['7dIncidence7dDiff'] > 75) {
    results.bad.count += 1;
    results.bad.population += gemeinde.properties.einwohner;
  }

  gemeinde.properties['7dIncidence'] = incidence ? incidence['value'] : -1;
  gemeinde.properties['7dInfections'] = infections ? infections['value'] : -1;

  if ( gemeinde.properties['7dIncidence'] < 15 ) {
    results.group1.count += 1;
    results.group1.population += gemeinde.properties.einwohner;
  }
  else if ( gemeinde.properties['7dIncidence'] >= 15 && gemeinde.properties['7dIncidence'] < 25) {
    results.group2.count += 1;
    results.group2.population += gemeinde.properties.einwohner;
  }
  else if ( gemeinde.properties['7dIncidence'] >= 25 && gemeinde.properties['7dIncidence'] < 35) {
    results.group3.count += 1;
    results.group3.population += gemeinde.properties.einwohner;
  }
  else if ( gemeinde.properties['7dIncidence'] >= 35 && gemeinde.properties['7dIncidence'] < 50) {
    results.group4.count += 1;
    results.group4.population += gemeinde.properties.einwohner;
  }
  else if ( gemeinde.properties['7dIncidence'] >= 50 && gemeinde.properties['7dIncidence'] < 100) {
    results.group5.count += 1;
    results.group5.population += gemeinde.properties.einwohner;
  }
  else if ( gemeinde.properties['7dIncidence'] >= 100 && gemeinde.properties['7dIncidence'] < 200) {
    results.group6.count += 1;
    results.group6.population += gemeinde.properties.einwohner;
  }
  else if ( gemeinde.properties['7dIncidence'] >= 200 && gemeinde.properties['7dIncidence'] < 350) {
    results.group7.count += 1;
    results.group7.population += gemeinde.properties.einwohner;
  }
  else if ( gemeinde.properties['7dIncidence'] >= 350 && gemeinde.properties['7dIncidence'] < 500) {
    results.group8.count += 1;
    results.group8.population += gemeinde.properties.einwohner;
  }
  else if ( gemeinde.properties['7dIncidence'] >= 500 && gemeinde.properties['7dIncidence'] < 1000) {
    results.group9.count += 1;
    results.group9.population += gemeinde.properties.einwohner;
  }
  else if ( gemeinde.properties['7dIncidence'] >= 1000) {
    results.group10.count += 1;
    results.group10.population += gemeinde.properties.einwohner;
  }
});

fs.writeFile('dist/json/sachsen_gemeinden.js', `var gemeinden = ${JSON.stringify(gemeinden)}`, (err) => {
  if (err) throw err;
});

replace.sync({ files: 'dist/index_in_progress.html', from: `GEMEINDE_DATUM_START`, to: moment(start).format("DD.MM.YYYY") });
replace.sync({ files: 'dist/index_in_progress.html', from: `GEMEINDE_DATUM_END`, to: moment(end).format("DD.MM.YYYY") });
replace.sync({ files: 'dist/index_in_progress.html', from: `GEMEINDE_DATUM`, to: moment(sql.getMaxDate("7d-incidence", "sms")).format("DD.MM.YYYY") });

replace.sync({ files: 'dist/index_in_progress.html', from: `GEMEINDEN_TO_15`, to: results.group1.count });
replace.sync({ files: 'dist/index_in_progress.html', from: `EINWOHNENDE_TO_15`, to: results.group1.population });

replace.sync({ files: 'dist/index_in_progress.html', from: `GEMEINDEN_TO_25`, to: results.group2.count });
replace.sync({ files: 'dist/index_in_progress.html', from: `EINWOHNENDE_TO_25`, to: results.group2.population });

replace.sync({ files: 'dist/index_in_progress.html', from: `GEMEINDEN_TO_35`, to: results.group3.count });
replace.sync({ files: 'dist/index_in_progress.html', from: `EINWOHNENDE_TO_35`, to: results.group3.population });

replace.sync({ files: 'dist/index_in_progress.html', from: `GEMEINDEN_TO_50`, to: results.group4.count });
replace.sync({ files: 'dist/index_in_progress.html', from: `EINWOHNENDE_TO_50`, to: results.group4.population });

replace.sync({ files: 'dist/index_in_progress.html', from: `GEMEINDEN_TO_100`, to: results.group5.count });
replace.sync({ files: 'dist/index_in_progress.html', from: `EINWOHNENDE_TO_100`, to: results.group5.population });

replace.sync({ files: 'dist/index_in_progress.html', from: `GEMEINDEN_TO_200`, to: results.group6.count });
replace.sync({ files: 'dist/index_in_progress.html', from: `EINWOHNENDE_TO_200`, to: results.group6.population });

replace.sync({ files: 'dist/index_in_progress.html', from: `GEMEINDEN_TO_350`, to: results.group7.count });
replace.sync({ files: 'dist/index_in_progress.html', from: `EINWOHNENDE_TO_350`, to: results.group7.population });

replace.sync({ files: 'dist/index_in_progress.html', from: `GEMEINDEN_TO_500`, to: results.group8.count });
replace.sync({ files: 'dist/index_in_progress.html', from: `EINWOHNENDE_TO_500`, to: results.group8.population });

replace.sync({ files: 'dist/index_in_progress.html', from: `GEMEINDEN_TO_1000`, to: results.group9.count });
replace.sync({ files: 'dist/index_in_progress.html', from: `EINWOHNENDE_TO_1000`, to: results.group9.population });

replace.sync({ files: 'dist/index_in_progress.html', from: `GEMEINDEN_TO_1500`, to: results.group10.count });
replace.sync({ files: 'dist/index_in_progress.html', from: `EINWOHNENDE_TO_1500`, to: results.group10.population });

replace.sync({ files: 'dist/index_in_progress.html', from: `GOOD_COUNT`, to: results.good.count });
replace.sync({ files: 'dist/index_in_progress.html', from: `GOOD_POPULATION`, to: results.good.population });

replace.sync({ files: 'dist/index_in_progress.html', from: `OKAY_COUNT`, to: results.okay.count });
replace.sync({ files: 'dist/index_in_progress.html', from: `OKAY_POPULATION`, to: results.okay.population });

replace.sync({ files: 'dist/index_in_progress.html', from: `SOSO_COUNT`, to: results.soso.count });
replace.sync({ files: 'dist/index_in_progress.html', from: `SOSO_POPULATION`, to: results.soso.population });

replace.sync({ files: 'dist/index_in_progress.html', from: `SAME_COUNT`, to: results.same.count });
replace.sync({ files: 'dist/index_in_progress.html', from: `SAME_POPULATION`, to: results.same.population });

replace.sync({ files: 'dist/index_in_progress.html', from: `WORSE_COUNT`, to: results.worse.count });
replace.sync({ files: 'dist/index_in_progress.html', from: `WORSE_POPULATION`, to: results.worse.population });

replace.sync({ files: 'dist/index_in_progress.html', from: `BAD_COUNT`, to: results.bad.count });
replace.sync({ files: 'dist/index_in_progress.html', from: `BAD_POPULATION`, to: results.bad.population });