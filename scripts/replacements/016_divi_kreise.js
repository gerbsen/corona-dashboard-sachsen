// https://www.arcgis.com/sharing/rest/content/items/f10774f1c63e40168479a1feb6c7ca74/data 
const replace = require('replace-in-file');
let fs = require("fs");
let path = require("path");
const _ = require('lodash');
const moment = require('moment');
var sql = require('../../scripts/sql.js');
const commons = require('./commons');

async function runKreise(){

  commons.kreise.filter(id => id != 14).forEach(function(kreis_id){

    const currentCovidCases = sql.getCurrentCovidValueFor("faelleCovidAktuell", kreis_id);
    const currentCovidCasesVentilated = sql.getCurrentCovidValueFor("faelleCovidAktuellBeatmet", kreis_id);
    const diviITSBedsFree = sql.getCurrentCovidValueFor("intensivBettenFrei", kreis_id);
    const diviITSBedsUsed = sql.getCurrentCovidValueFor("intensivBettenBelegt", kreis_id);

    // console.log(kreis_id)
    // console.log(currentCovidCases)
    // console.log(currentCovidCasesVentilated)
    // console.log(diviITSBedsFree)
    // console.log(diviITSBedsUsed)

    var template = fs.readFileSync(path.join(__dirname, "../../html/divi_kreise_template.js"), "utf8");

    template = template.replace(/DIVI_LABELS/g, Object.keys(currentCovidCases).map(date => `"${moment(date).format("DD.MM.")}"`).join());
    template = template.replace(/DIVI_LABEL/g, "COVID-19 Fälle an Krankenhäusern (RKI)");
    template = template.replace(/DATA_1/g, Object.values(currentCovidCases).join());
    template = template.replace(/DATA_LABEL_1/g, "Fälle COVID-19 aktuell in Behandlung");
    template = template.replace(/DATA_2/g, Object.values(currentCovidCasesVentilated).join());
    template = template.replace(/DATA_LABEL_2/g, "Fälle COVID-19 aktuell invasiv beatmet");
    fillTemplate(kreis_id, `diviCovidCases${kreis_id}`, template);

    var template = fs.readFileSync(path.join(__dirname, "../../html/divi_kreise_template.js"), "utf8");
    template = template.replace(/DIVI_LABELS/g, Object.keys(diviITSBedsFree).map(date => `"${moment(date).format("DD.MM.")}"`).join());
    template = template.replace(/DIVI_LABEL/g, "Belegung Intensivbetten (RKI)");
    template = template.replace(/DATA_1/g, Object.values(diviITSBedsFree).join());
    template = template.replace(/DATA_LABEL_1/g, "Intensivbetten frei");
    template = template.replace(/DATA_2/g, Object.values(diviITSBedsUsed).join());
    template = template.replace(/DATA_LABEL_2/g, "Intensivbetten belegt");
    fillTemplate(kreis_id, `diviITSBeds${kreis_id}`, template);
  });
}

function fillTemplate(kreis_id, canvasName, template){
  
  var onloadHtml = `
    var ctx = document.getElementById('${canvasName}Canvas').getContext('2d');
    window.canvas_age_incidence_${kreis_id} = new Chart(ctx, ${template});
    /* WINDOW_ONLOAD_NEEDLE */`;

  replace.sync({ files: 'dist/index_in_progress.html', from: `/* WINDOW_ONLOAD_NEEDLE */`, to: onloadHtml });
}

runKreise();