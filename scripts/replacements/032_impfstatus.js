const replace = require('replace-in-file');
const fs = require("fs");
const moment = require('moment');
const _ = require('lodash');
const parse = require('csv-parse/lib/sync');
const commons = require('./commons');

// Impfdatum,LandkreisId_Impfort,Altersgruppe,Impfschutz,Anzahl
// 2020-12-27,14524,18-59,1,10
// 2020-12-27,14524,60+,1,57
// 2020-12-27,14625,18-59,1,19
// 2020-12-27,14625,60+,1,60
// 2020-12-28,14511,18-59,1,159
async function run(){
  
  const content = await fs.promises.readFile(`dist/Aktuell_Deutschland_Landkreise_COVID-19-Impfungen.Sachsen.csv`)
  // Parse the CSV content
  const records = parse(content, {columns : true});
  const saxony = { 'shots_1_12-17' : {}, 'shots_1_18-59' : {}, 'shots_1_60+' : {},
                   'shots_2_12-17' : {}, 'shots_2_18-59' : {}, 'shots_2_60+' : {},
                   'shots_3_12-17' : {}, 'shots_3_18-59' : {}, 'shots_3_60+' : {}};

  const dates = [...new Set(records.map(record => record.Impfdatum))];
  const maxDate = dates.reduce(function (a, b) { return new Date(a) > new Date(b) ? a : b; });

  // schema of the file
  // "Impfdatum,LandkreisId_Impfort,Altersgruppe,Impfschutz,Anzahl"
  Object.values(commons.kreisIdToAgsMapping).forEach(function(ags){
    
    const recordsByLandkreis = records.filter(item => item.LandkreisId_Impfort == ags);
    const landkreis = { 'shots_1_12-17' : {}, 'shots_1_18-59' : {}, 'shots_1_60+' : {},
                        'shots_2_12-17' : {}, 'shots_2_18-59' : {}, 'shots_2_60+' : {},
                        'shots_3_12-17' : {}, 'shots_3_18-59' : {}, 'shots_3_60+' : {}};

    dates.forEach(function(date){
      const recordsByLandkreisAndDate = recordsByLandkreis.filter(record => record.Impfdatum == date);

      [1,2,3].forEach(function(shotNumber){
        ['12-17','18-59','60+'].forEach(function(ageGroup){
          landkreis['shots_' + shotNumber + '_' + ageGroup][date] = recordsByLandkreisAndDate
            .filter(record => record.Impfschutz == shotNumber && record.Altersgruppe == ageGroup)
            .reduce((total, item) => Number(item.Anzahl) + total, 0);
        })
      })
    })

    // sum for view of all saxony / 14
    sumShots(saxony, landkreis);
    
    replace.sync({ files: 'dist/index_in_progress.html', from: `/* WINDOW_ONLOAD_NEEDLE */`, to: getChart(commons.agsToKreisIdMapping[ags], landkreis, dates) });
    replace.sync({ files: 'dist/index_in_progress.html', from: `VACCINATIONS_OVERVIEW_${commons.agsToKreisIdMapping[ags]}`, to: getProgressBarsHtml(returnData(recordsByLandkreis, landkreis, ags, maxDate), maxDate)});   
  });
  
  replace.sync({ files: 'dist/index_in_progress.html', from: `/* WINDOW_ONLOAD_NEEDLE */`, to: getChart(14, saxony, dates) });
  replace.sync({ files: 'dist/index_in_progress.html', from: `VACCINATIONS_OVERVIEW_SACHSEN`, to: getProgressBarsHtml(returnData(records, saxony, 14, maxDate), maxDate)});   
}

function returnData(recordsByLandkreis, landkreis, ags, maxDate) {
  
  const data = {};
  ['12-17','18-59','60+'].forEach(function(ageGroup){
    data[ageGroup] = {};
    data[ageGroup].population = commons.populationByAgeClass[commons.agsToKreisIdMapping[ags]][ageGroup],
    [1, 2, 3].forEach(function(shotNumber){
      data[ageGroup][shotNumber + '-shots-total'] = Object.values(landkreis['shots_'+shotNumber+'_' + ageGroup]).reduce((a, b) => a + b);
      data[ageGroup][shotNumber + '-shots-total-percent'] = _.round((data[ageGroup][shotNumber + '-shots-total'] / data[ageGroup].population) * 100, 1);
      data[ageGroup][shotNumber + '-shots-7d' ]   = recordsByLandkreis
        .filter(record => 
          moment(record.Impfdatum).isSameOrBefore(moment(maxDate)) && 
          moment(record.Impfdatum).isAfter(moment(maxDate).subtract(7, 'days')) &&
          record.Altersgruppe == ageGroup &&
          record.Impfschutz == shotNumber)
        .reduce((total, item) => Number(item.Anzahl) + total, 0);
      data[ageGroup][shotNumber + '-shots-daily-avg-last-7d'] = data[ageGroup][shotNumber + '-shots-7d' ] / 7;
      data[ageGroup][shotNumber + '-shots-days-left-100p'] = 
        (data[ageGroup].population - data[ageGroup][shotNumber + '-shots-total']) / (data[ageGroup][shotNumber + '-shots-daily-avg-last-7d']);
    })  
  });

  return data;
}

function sumShots(saxony, landkreis) {

  [1,2,3].forEach(function(shotNumber){
    ['12-17','18-59','60+'].forEach(function(ageGroup){

      const index = "shots_" + shotNumber + '_' + ageGroup;

      Object.keys(landkreis[index]).forEach(function(date) {
        if ( !(date in saxony[index]) ) 
          saxony[index][date] = landkreis[index][date];
        else 
          saxony[index][date] += landkreis[index][date];
      })
    })
  })
}

function getProgressBarsHtml(data, maxDate){

  var html = `
  <div class="row mt-4">
    <h3>Schätzung der Impfquote je Altersklasse
  </div>
  <div class="row mt-4">
    <div class="col-md-1 text-center">
      Alter
    </div>
    <div class="col text-center">
      Erstimpfung
    </div>
    <div class="col text-center">
      Zweitimpfung
    </div>
    <div class="col text-center">
      Drittimpfung
    </div>
  </div>`;

  ['12-17','18-59','60+'].forEach(function(ageGroup){

    html += 
    `<div class="row mt-3">`
    html += 
      `<div class="col-md-1 text-center">
        ${ageGroup}
      </div>`;

    [1,2,3].forEach(function(shotNumber){
      html += 
      `<div class="col text-center">
        <div class="row">
          <div class="col-md-12">
            <div class="progress" style="height: 30px;">
              <div class="progress-bar" role="progressbar" style="width: ${data[ageGroup][shotNumber + '-shots-total-percent']}%;" aria-valuenow="${data[ageGroup][shotNumber + '-shots-total-percent']}" aria-valuemin="0" aria-valuemax="100">${data[ageGroup][shotNumber + '-shots-total-percent']}%</div>
            </div>
          </div>
        </div>
        <div class="row m-3">
          <table class="table table-sm table-striped table-borderless">
            <tbody>
              <tr>
                <td><i class="fas fa-syringe"></i> pro Tag </td>
                <td>∅ ${_.round(data[ageGroup][shotNumber + '-shots-daily-avg-last-7d'], 0)}</td>
              </tr>
              <tr>
                <td>Geimpfte</td>
                <td>${data[ageGroup][shotNumber + '-shots-total'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')}</td>
              </tr>
              <tr>
                <td>Gesamt</td>
                <td>ca. ${data[ageGroup].population.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')}</td>
              </tr>
              <tr>
                <td>Tage bis 100%</td>
                <td>${_.round(data[ageGroup][shotNumber + '-shots-days-left-100p'],0)}</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>`;
    });

    html += '</div>'
  });

  html += `<div class="row mt-3">
    <div class="col-md-12">
      <p>
      Die Daten enthalten alle an das RKI bis zum ${moment(maxDate).format("DD.MM.YYYY")} gemeldeten Impfungen. 
      Für den Durchschnitt werden alle gemeldeten Impfungen der letzten 7 Tage (bis ${moment(maxDate).subtract(7, 'days').format("DD.MM.YYYY")}) verwendet. <br/>
      Die Altersklassen für Impfungen werden in den Kohorten (12-17,18-59,60+) erfasst. 
      Für diese Altersklassen ist aber die Gesamtzahl an Personen pro Klasse nicht öffentlich verfügbar. 
      Durch die Hilfe von <a href="https://twitter.com/openclimatedata">Robert Gieseke</a> war es aber möglich eine
      ungefähre Anzahl an Personen pro Klasse aus SurvStat <b>zu schätzen</b>.
      Die Impfdaten werden nur für den Impfort und nicht für den Wohnort erhoben.
      </p>
    </div>
  </div>`;

  return html;
}

function getChart(kreis_id, values, dates){

  const dateFormat = "DD.MM.YYYY";
  const sortedAndFormatedDates = _.map(dates, date => new Date(date))
                                  .sort((date1, date2) => date1 - date2)
                                  .map(b => moment(b).format(dateFormat));

  return `
    var ctx = document.getElementById('kreisChart${kreis_id}ImpfstatusCanvas').getContext('2d');
    window.kreisChart${kreis_id}ImpfstatusCanvasChart = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: ${JSON.stringify(sortedAndFormatedDates)},
        datasets: [
          {
            label: 'Erstimpfung (12-17 Jahre)',
            backgroundColor: '#FA8072',
            data: ${JSON.stringify(_.map(values['shots_1_12-17'], function(value, key){ return { x: moment(key).format(dateFormat), y: value} }))}
          },
          {
            label: 'Erstimpfung (18-59 Jahre)',
            backgroundColor: '#F08080',
            data: ${JSON.stringify(_.map(values['shots_1_18-59'], function(value, key){ return { x: moment(key).format(dateFormat), y: value} }))}
          },
          {
            label: 'Erstimpfung (60+ Jahre)',
            backgroundColor: '#CD5C5C',
            data: ${JSON.stringify(_.map(values['shots_1_60+'], function(value, key){ return { x: moment(key).format(dateFormat), y: value} }))}
          },
          {
            label: 'Zweitimpfung (12-17 Jahre)',
            backgroundColor: '#00BFFF',
            data: ${JSON.stringify(_.map(values['shots_2_12-17'], function(value, key){ return { x: moment(key).format(dateFormat), y: value} }))}
          },
          {
            label: 'Zweitimpfung (18-59 Jahre)',
            backgroundColor: '#1E90FF',
            data: ${JSON.stringify(_.map(values['shots_2_18-59'], function(value, key){ return { x: moment(key).format(dateFormat), y: value} }))}
          },
          {
            label: 'Zweitimpfung (60+ Jahre)',
            backgroundColor: '#00008B',
            data: ${JSON.stringify(_.map(values['shots_2_60+'], function(value, key){ return { x: moment(key).format(dateFormat), y: value} }))}
          },
          {
            label: 'Drittimpfung (12-17 Jahre)',
            backgroundColor: '#EE82EE',
            data: ${JSON.stringify(_.map(values['shots_3_12-17'], function(value, key){ return { x: moment(key).format(dateFormat), y: value} }))}
          },
          {
            label: 'Drittimpfung (18-59 Jahre)',
            backgroundColor: '#BA55D3',
            data: ${JSON.stringify(_.map(values['shots_3_18-59'], function(value, key){ return { x: moment(key).format(dateFormat), y: value} }))}
          },
          {
            label: 'Drittimpfung (60+ Jahre)',
            backgroundColor: '#8B008B',
            data: ${JSON.stringify(_.map(values['shots_3_60+'], function(value, key){ return { x: moment(key).format(dateFormat), y: value} }))}
          }
      ]
      },
      options: {
        responsive: true,
        title: { display: false },
        interaction: { mode: 'x' },
        tooltips: { mode: 'index', intersect: false },
        hover: { mode: 'nearest', intersect: true },
        legend: { position: 'bottom' },
        scales: {
          x: {
            display: true,
            stacked: true,
            scaleLabel: {
              display: true,
              labelString: 'Datum'
            }
          },
          y: {
            display: true,
            stacked: true,
            position: 'left',
            scaleLabel: {
              display: true,
              labelString: 'Anzahl an täglichen Impfungen'
            },
            gridLines: {
              display: true,
              drawBorder: true,
              drawOnChartArea: true,
              drawTicks: true,
            }
          }
        }
      }
    });
    
    /* WINDOW_ONLOAD_NEEDLE */`
}

run();
