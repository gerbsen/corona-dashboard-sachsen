const replace = require('replace-in-file');
const _ = require('lodash');
const moment = require('moment');
var commons = require('../../scripts/replacements/commons.js');
var sql = require('../../scripts/sql.js');

let diviDate = -1;
var infectionMax = -1, infectionMin = 100000000;
var incidenceMax = -1, incidenceMin = 100000000;
var freeBedsRatioMax = -1, freeBedsRatioMin = 100000000;

let totalFreeITSBeds = 0, totalUsedITSBeds = 0;

commons.kreise.forEach(kreis_id => {

  const sumCases = sql.getInfectionsForDistrict(kreis_id);
  const incidence = sql.getXDaysIncidenceForDistrict(kreis_id, 7, 0);
  let freeBedsRatio = -1;
    
  if ( kreis_id != 14 ) { 
    
    diviDate = sql.getSingleCovidValueFor("intensivBettenFrei", kreis_id, 1).date;
    const intensivBettenFrei = sql.getSingleCovidValueFor("intensivBettenFrei", kreis_id, 1).value;
    const intensivBettenBelegt = sql.getSingleCovidValueFor("intensivBettenBelegt", kreis_id, 1).value;
    freeBedsRatio = ((intensivBettenFrei / (intensivBettenBelegt + intensivBettenFrei + 0.0)) * 100).toFixed(1);

    totalFreeITSBeds += intensivBettenFrei;
    totalUsedITSBeds += intensivBettenBelegt;

    infectionMax = Math.max(infectionMax, sumCases);
    infectionMin = Math.min(infectionMin, sumCases);
    incidenceMax = Math.max(incidenceMax, incidence);
    incidenceMin = Math.min(incidenceMin, incidence);
    freeBedsRatioMax = Math.max(freeBedsRatioMax, freeBedsRatio);
    freeBedsRatioMin = Math.min(freeBedsRatioMin, freeBedsRatio);
  }

  replace.sync({ files: 'dist/json/sachsen.js', 
    from: `KREIS_ID_${kreis_id}_NEEDLE`, 
    to: `"infections": ${sumCases}, "incidence": ${incidence}, "freeBedsRatio": ${freeBedsRatio}, ` });
})

replace.sync({ files: 'dist/index_in_progress.html', from: `INFECTIONS_MAX`, to: infectionMax });
replace.sync({ files: 'dist/index_in_progress.html', from: `INFECTIONS_MIN`, to: infectionMin });
replace.sync({ files: 'dist/index_in_progress.html', from: `INCIDENCE_MAX`, to: incidenceMax });
replace.sync({ files: 'dist/index_in_progress.html', from: `INCIDENCE_MIN`, to: incidenceMin });
replace.sync({ files: 'dist/index_in_progress.html', from: /FREE_BEDS_RATION_MAX/g, to: freeBedsRatioMax });
replace.sync({ files: 'dist/index_in_progress.html', from: /FREE_BEDS_RATION_MIN/g, to: freeBedsRatioMin });
replace.sync({ files: 'dist/index_in_progress.html', from: /TOTAL_ITS_BEDS_USED/g, to: totalUsedITSBeds });
replace.sync({ files: 'dist/index_in_progress.html', from: /DIVI_DATUM/g, to: moment(diviDate).format("DD.MM.YYYY") });
replace.sync({ files: 'dist/index_in_progress.html', from: /TOTAL_ITS_BEDS_FREE_RATIO/g, 
  to: ((totalFreeITSBeds / (totalUsedITSBeds + totalFreeITSBeds + 0.0)) * 100).toFixed(1) });
replace.sync({ files: 'dist/index_in_progress.html', from: /TOTAL_ITS_BEDS_FREE/g, to: totalFreeITSBeds });