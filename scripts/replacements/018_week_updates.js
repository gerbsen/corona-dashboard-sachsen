// https://www.arcgis.com/sharing/rest/content/items/f10774f1c63e40168479a1feb6c7ca74/data 
const replace = require('replace-in-file');
let fs = require("fs");
let path = require("path");
const _ = require('lodash');
const moment = require('moment');
var sql = require('../../scripts/sql.js');
const commons = require('./commons');

async function runKreise(){

  commons.kreise.forEach(function(kreis_id){

    const infectedWeekValues = sql.getWeekValues(kreis_id, "infected").reverse();
    const deadWeekValues     = sql.getWeekValues(kreis_id, "death").reverse();
    
    var template = fs.readFileSync(path.join(__dirname, "../../html/week_updates_template.js"), "utf8");
    template = template.replace(/GRAPH_TITLE/g, "Infektion pro Woche (RKI)");
    template = template.replace(/GRAPH_DATASET_LABEL/g, "Infektionen");
    template = template.replace(/Y-AXIS_LABEL/g, "Anzahl");
    template = template.replace(/CHART_COLOR/g, "window.chartColors.red");
    template = template.replace(/LABELS/g, infectedWeekValues.map(row => `"${row.year=='2021'?'21-':''}${row.week}"`).join());
    template = template.replace(/DATA/g, infectedWeekValues.map(row => row.sum).join());
    fillTemplate(kreis_id, `infectedWeekValues${kreis_id}`, template);

    var template = fs.readFileSync(path.join(__dirname, "../../html/week_updates_template.js"), "utf8");
    template = template.replace(/GRAPH_TITLE/g, "Todesfälle pro Woche (RKI)");
    template = template.replace(/GRAPH_DATASET_LABEL/g, "Todesfälle");    
    template = template.replace(/Y-AXIS_LABEL/g, "Anzahl");
    template = template.replace(/CHART_COLOR/g, "window.chartColors.black");
    template = template.replace(/LABELS/g, deadWeekValues.map(row => `"${row.week}"`).join());
    template = template.replace(/DATA/g, deadWeekValues.map(row => row.sum).join());
    fillTemplate(kreis_id, `deathWeekValues${kreis_id}`, template);
  });
}

function fillTemplate(kreis_id, canvasName, template){
  
  var onloadHtml = `
    var ctx = document.getElementById('${canvasName}Canvas').getContext('2d');
    window.canvas_age_incidence_${kreis_id} = new Chart(ctx, ${template});
    /* WINDOW_ONLOAD_NEEDLE */`;

  replace.sync({ files: 'dist/index_in_progress.html', from: `/* WINDOW_ONLOAD_NEEDLE */`, to: onloadHtml });
}

runKreise();