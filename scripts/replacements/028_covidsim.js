const replace = require('replace-in-file');
const fs = require("fs");
const path = require("path");
const moment = require('moment');
const _ = require('lodash');
const sql = require('../../scripts/sql.js');
const helper = require('../../scripts/helper.js');
const commons = require('./commons');
const csv = require('../../scripts/csv.js');


const numberOfDays = commons.days;

async function run(){

  const rows = (await csv.getRows('dist/covidsim/cosim-Sachsen-current.csv'))
    .filter(item => _.startsWith(item.var, 'DAILY') || item.var == 'INCIDENCE');
  const labels = new Set();
  const data = { INCIDENCE: [], DAILY_CASES: [] , DAILY_DEAD: [] , DAILY_RECOVERED: [] };

  // group all the values to the districts
  rows.forEach(row => {
    labels.add("\"" + moment(row.date).format("D.M.")+ "\"" )  
    data[row.var].push({
      y: Number(_.round(row.meansim, 1)),
      yMin: Number(_.round(row.minsim, 1)),
      yMax: Number(_.round(row.maxsim, 1)),
    })
  });

  var onloadHtml = `
    var ctx = document.getElementById('covidsim-incidence-canvas-14').getContext('2d');
    window.covidsimCanvas14 = new Chart(ctx, {
      type: 'barWithErrorBars',
      data: {
        labels: [${Array.from(labels).join()}],
        datasets: [{
          label: 'Inzidenz für Sachsen',
          backgroundColor: window.chartColors.red,
          borderColor: window.chartColors.red,
          data: ${JSON.stringify(data.INCIDENCE)},
          fill: false,
          hidden: false
        }]
      },
      options: {
        responsive: true,
        title: { display: false },
        tooltips: { mode: 'index', intersect: false },
        hover: { mode: 'nearest', intersect: true },
        legend: { position: 'bottom' },
        scales: {
          xAxis: {
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'Datum'
            }
          },
          yAxis: {
            display: true,
            position: 'right',
            scaleLabel: {
              display: true,
              labelString: 'Inzidenz'
            },
            gridLines: {
              display: true,
              drawBorder: true,
              drawOnChartArea: true,
              drawTicks: true,
            }
          }
        }
      }
    });
    var ctx = document.getElementById('covidsim-cases-canvas-14').getContext('2d');
    window.covidsimCasesCanvas14 = new Chart(ctx, {
      type: 'barWithErrorBars',
      data: {
        labels: [${Array.from(labels).join()}],
        datasets: [{
          label: 'Neuinfektionen',
          backgroundColor: window.chartColors.red,
          borderColor: window.chartColors.red,
          data: ${JSON.stringify(data.DAILY_CASES)},
          fill: false,
          hidden: false
        },{
          label: 'Verstorbene',
          backgroundColor: window.chartColors.black,
          borderColor: window.chartColors.black,
          data: ${JSON.stringify(data.DAILY_DEAD)},
          fill: false,
          hidden: false
        },{
          label: 'Genesene',
          backgroundColor: window.chartColors.green,
          borderColor: window.chartColors.green,
          data: ${JSON.stringify(data.DAILY_RECOVERED)},
          fill: false,
          hidden: false
        }]
      },
      options: {
        responsive: true,
        title: { display: false },
        tooltips: { mode: 'index', intersect: false },
        hover: { mode: 'nearest', intersect: true },
        legend: { position: 'bottom' },
        scales: {
          xAxis: {
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'Datum'
            }
          },
          yAxis: {
            display: true,
            position: 'right',
            scaleLabel: {
              display: true,
              labelString: 'Inzidenz'
            },
            gridLines: {
              display: true,
              drawBorder: true,
              drawOnChartArea: true,
              drawTicks: true,
            }
          }
        }
      }
    });
    /* WINDOW_ONLOAD_NEEDLE */`
  replace.sync({ files: 'dist/index_in_progress.html', from: `/* WINDOW_ONLOAD_NEEDLE */`, to: onloadHtml });
}

run();
