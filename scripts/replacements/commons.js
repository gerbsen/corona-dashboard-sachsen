const fs = require("fs");
const path = require("path");
const https = require('https');

const download = function(url, dest, cb) {
  var file = fs.createWriteStream(dest);
  var request = https.get(url, function(response) {
    response.pipe(file);
    file.on('finish', function() {
      file.close(cb);  // close() is async, call cb after close completes.
    });
  }).on('error', function(err) { // Handle errors
    fs.unlink(dest); // Delete the file async. (But we don't check the result)
    if (cb) cb(err.message);
  });
};

module.exports = {
  download : download,
  // kreise : [12],
  kreise : [1,2,3,4,5,6,7,8,9,10,11,12,13,14],
  hospitalizationQuote: 0.1,
  sourceForInfections: 'rki',
  days : 100,
  ageGroups: ['A00-A04', 'A05-A14', 'A15-A34', 'A35-A59', 'A60-A79', 'A80+'],
  rkiNamesMapping: {
    'SK Dresden' : '1',
    'SK Chemnitz' : '2',
    'SK Leipzig' : '3',
    'LK Bautzen' : '4',
    'LK Erzgebirgskreis' : '5',
    'LK Görlitz' : '6',
    'LK Leipzig' : '7',
    'LK Meißen' : '8',
    'LK Mittelsachsen' : '9',
    'LK Nordsachsen' : '10',
    'LK Sächsische Schweiz-Osterzgebirge' : '11',
    'LK Vogtlandkreis' : '12',
    'LK Zwickau' : '13',
    'Sachsen' : '14'
  },
  slugs : {
    '1' : 'dresden',
    '2' : 'chemnitz',
    '3' : 'leipzig',
    '4' : 'bautzen',
    '5' : 'erzgebirge',
    '6' : 'goerlitz',
    '7' : 'landkreis-leipzig',
    '8' : 'meissen',
    '9' : 'mittelsachsen',
    '10' : 'nordsachsen',
    '11' : 'saechsische-schweiz-osterzgebirge',
    '12' : 'vogtlandkreis',
    '13' : 'zwickau',
    '14' : 'sachsen'
  },
  names : {
    '1' : 'Dresden',
    '2' : 'Chemnitz',
    '3' : 'Leipzig',
    '4' : 'Bautzen',
    '5' : 'Erzgebirgskreis',
    '6' : 'Görlitz',
    '7' : 'Landkreis Leipzig',
    '8' : 'Meißen',
    '9' : 'Mittelsachsen',
    '10' : 'Nordsachsen',
    '11' : 'Sächsische Schweiz-Osterzgebirge',
    '12' : 'Vogtlandkreis',
    '13' : 'Zwickau',
    '14' : 'Sachsen'
  },
  populationByAgeClass : {
    '1'  : { '12-17' : 27813,  '18-59': 309432,  '60+': 151722,  'total': 551072,  'A00-A04': 29658,  'A05-A14': 51935,  'A15-A34': 148323, 'A35-A59': 175921,  'A60-A79': 109387,  'A80+': 41556 },
    '2'  : { '12-17' : 11553,  '18-59': 122588,  '60+': 84458,   'total': 246855,  'A00-A04': 11427,  'A05-A14': 20414,  'A15-A34': 53655,  'A35-A59': 76096,   'A60-A79': 62793,   'A80+': 21949 },
    '3'  : { '12-17' : 26889,  '18-59': 349053,  '60+': 151889,  'total': 581980,  'A00-A04': 31764,  'A05-A14': 50974,  'A15-A34': 171004, 'A35-A59': 188265,  'A60-A79': 110384,  'A80+': 40754 },
    '4'  : { '12-17' : 15936,  '18-59': 140683,  '60+': 109013,  'total': 302634,  'A00-A04': 12995,  'A05-A14': 27522,  'A15-A34': 46285,  'A35-A59': 104593,  'A60-A79': 80743,   'A80+': 27620 },
    '5'  : { '12-17' : 17686,  '18-59': 151951,  '60+': 128529,  'total': 340373,  'A00-A04': 13454,  'A05-A14': 29456,  'A15-A34': 51850,  'A35-A59': 111700,  'A60-A79': 97914,   'A80+': 30574 },
    '6'  : { '12-17' : 13048,  '18-59': 114840,  '60+': 97600,   'total': 256587,  'A00-A04': 9935,   'A05-A14': 21846,  'A15-A34': 39452,  'A35-A59': 84118,   'A60-A79': 72502,   'A80+': 24872 },
    '7'  : { '12-17' : 13773,  '18-59': 125267,  '60+': 91024,   'total': 258008,  'A00-A04': 11286,  'A05-A14': 23658,  'A15-A34': 40234,  'A35-A59': 93143,   'A60-A79': 68375,   'A80+': 21443 },
    '8'  : { '12-17' : 13214,  '18-59': 114316,  '60+': 86825,   'total': 242862,  'A00-A04': 10303,  'A05-A14': 22306,  'A15-A34': 37965,  'A35-A59': 84906,   'A60-A79': 63925,   'A80+': 22312 },
    '9'  : { '12-17' : 15269,  '18-59': 144043,  '60+': 111090,  'total': 308153,  'A00-A04': 12498,  'A05-A14': 26343,  'A15-A34': 51167,  'A35-A59': 103044,  'A60-A79': 83068,   'A80+': 27979 },
    '10' : { '12-17' : 10241,  '18-59': 97106,   '60+': 68880,   'total': 197794,  'A00-A04': 8681,   'A05-A14': 17392,  'A15-A34': 32412,  'A35-A59': 71342,   'A60-A79': 51551,   'A80+': 16363},
    '11' : { '12-17' : 13678,  '18-59': 116680,  '60+': 86722,   'total': 245418,  'A00-A04': 11047,  'A05-A14': 23410,  'A15-A34': 39650,  'A35-A59': 85059,   'A60-A79': 64330,   'A80+': 22090 },
    '12' : { '12-17' : 11048,  '18-59': 104156,  '60+': 87392,   'total': 229584,  'A00-A04': 8712,   'A05-A14': 18327,  'A15-A34': 35778,  'A35-A59': 75937,   'A60-A79': 65559,   'A80+': 21684 },
    '13' : { '12-17' : 15582,  '18-59': 147441,  '60+': 117963,  'total': 319998,  'A00-A04': 12662,  'A05-A14': 26077,  'A15-A34': 52540,  'A35-A59': 105694,  'A60-A79': 88468,   'A80+': 29561 },
    '14' : { '12-17' : 205730, '18-59': 2037556, '60+': 1373107, 'total': 4077937, 'A00-A04': 184422, 'A05-A14': 359660, 'A15-A34': 800315, 'A35-A59': 1359818, 'A60-A79': 1018999, 'A80+': 348757 },
  },
  population : {
    '1'  : 551072,
    '2'  : 246855,
    '3'  : 581980,
    '4'  : 302634,
    '5'  : 340373,
    '6'  : 256587,
    '7'  : 258008,
    '8'  : 242862,
    '9'  : 308153,
    '10' : 197794,
    '11' : 245418,
    '12' : 229584,
    '13' : 319998,
    '14' : 4077937
  },
  agsToKreisIdMapping : {
    "14612": 1,
    "14511": 2,
    "14713": 3,
    "14625": 4,
    "14521": 5,
    "14626": 6,
    "14729": 7,
    "14627": 8,
    "14522": 9,
    "14730": 10,
    "14628": 11,
    "14523": 12,
    "14524": 13,
    "14": 14
  },
  kreisIdToAgsMapping : {
    1 : "14612",
    2 : "14511",
    3 : "14713",
    4 : "14625",
    5 : "14521",
    6 : "14626",
    7 : "14729",
    8 : "14627",
    9 : "14522",
    10 : "14730",
    11 : "14628",
    12 : "14523",
    13 : "14524"
  },
  colors : {
    '1'  : '#EF767A',
    '2'  : '#456990',
    '3'  : '#49BEAA',
    '4'  : '#49DCB1',
    '5'  : '#EEB868',
    '6'  : '#DB5ABA',
    '7'  : '#CF8BA3',
    '8'  : '#E5CDC8',
    '9'  : '#0B3954',
    '10' : '#BFD7EA',
    '11' : '#FF6663',
    '12' : '#E0FF4F',
    '13' : '#9FFFF5',
    '14' : '#0E34A0'
  },
  diviKeys : {
    anzahl_meldebereiche: 'anzahl_meldebereiche',
    faelle_covid_aktuell: 'faelleCovidAktuell',
    faelle_covid_aktuell_beatmet: 'faelleCovidAktuellBeatmet',
    faelle_covid_aktuell_invasiv_beatmet: 'faelleCovidAktuellBeatmet',
    anzahl_standorte: 'anzahl_standorte',
    betten_frei: 'intensivBettenFrei',
    betten_belegt: 'intensivBettenBelegt',
  },
  importantDates : [
    // {
    //   type: "line",
    //   mode: "vertical",
    //   scaleID: "x-axis-0",
    //   value: "07.11.",
    //   borderColor: "rgb(75, 192, 192)",
    //   borderWidth: 4,
    //   label: {
    //     content: "#LE0711",
    //     enabled: true,
    //     position: "top",
    //     rotation: 0
    //   }
    // },
    // {
    //   type: "line",
    //   mode: "vertical",
    //   scaleID: "x-axis-0",
    //   value: "13.11.",
    //   borderColor: "rgb(75, 192, 192)",
    //   borderWidth: 4,
    //   label: {
    //     content: "SächsCoronaSchVO",
    //     enabled: true,
    //     position: "top",
    //     rotation: 0
    //   }
    // },
    // {
    //   type: "line",
    //   mode: "vertical",
    //   scaleID: "x-axis-0",
    //   value: "01.11.",
    //   borderColor: "rgb(75, 192, 192)",
    //   borderWidth: 4,
    //   label: {
    //     content: "Gastro/Kultur geschlossen",
    //     enabled: true,
    //     position: "top",
    //     rotation: 0
    //   }
    // },
    // {
    //   type: "line",
    //   mode: "vertical",
    //   scaleID: "x-axis-0",
    //   value: "01.12.",
    //   borderColor: "rgb(75, 192, 192)",
    //   borderWidth: 4,
    //   label: {
    //     content: "Kontaktbeschränkung",
    //     enabled: true,
    //     position: "top",
    //     rotation: 0
    //   }
    // },
    // {
    //   type: "line",
    //   mode: "vertical",
    //   scaleID: "x-axis-0",
    //   value: "14.12.",
    //   borderColor: "rgb(75, 192, 192)",
    //   borderWidth: 4,
    //   label: {
    //     content: "Lockdown2",
    //     enabled: true,
    //     position: "top",
    //     rotation: 0
    //   }
    // },
    // {
    //   type: "line",
    //   mode: "vertical",
    //   scaleID: "x-axis-0",
    //   value: "24.12.",
    //   borderColor: "rgb(75, 192, 192)",
    //   borderWidth: 4,
    //   label: {
    //     content: "Weihnachten",
    //     enabled: true,
    //     position: "top",
    //     rotation: 0
    //   }
    // },
    // {
    //   type: "line",
    //   mode: "vertical",
    //   scaleID: "x-axis-0",
    //   value: "31.12.",
    //   borderColor: "rgb(75, 192, 192)",
    //   borderWidth: 4,
    //   label: {
    //     content: "Silvester",
    //     enabled: true,
    //     position: "top",
    //     rotation: 0
    //   }
    // },
    // {
    //   type: "line",
    //   mode: "vertical",
    //   scaleID: "x-axis-0",
    //   value: "18.01.",
    //   borderColor: "rgb(75, 192, 192)",
    //   borderWidth: 4,
    //   label: {
    //     content: "Abschlussklassen",
    //     enabled: true,
    //     position: "top",
    //     rotation: 0
    //   }
    // },
    // {
    //   type: "line",
    //   mode: "vertical",
    //   scaleID: "x-axis-0",
    //   value: "15.02.",
    //   borderColor: "rgb(75, 192, 192)",
    //   borderWidth: 4,
    //   label: {
    //     content: "Grundschulen und Kitas",
    //     enabled: true,
    //     position: "top",
    //     rotation: 0
    //   }
    // },
    // {
    //   type: "line",
    //   mode: "vertical",
    //   scaleID: "x-axis-0",
    //   value: "08.03.",
    //   borderColor: "rgb(75, 192, 192)",
    //   borderWidth: 4,
    //   label: {
    //     content: "Zwei Haushalte",
    //     enabled: true,
    //     position: "bottom",
    //     rotation: 0
    //   }
    // },
    // {
    //   type: "line",
    //   mode: "vertical",
    //   scaleID: "x-axis-0",
    //   value: "15.03.",
    //   borderColor: "rgb(75, 192, 192)",
    //   borderWidth: 4,
    //   label: {
    //     content: "Wechselmodell",
    //     enabled: true,
    //     position: "top",
    //     rotation: 0
    //   }
    // },
    // {
    //   type: "line",
    //   mode: "vertical",
    //   scaleID: "x-axis-0",
    //   value: "02.04.",
    //   borderColor: "rgb(75, 192, 192)",
    //   borderWidth: 4,
    //   label: {
    //     content: "Ostern",
    //     enabled: true,
    //     position: "top",
    //     rotation: 0
    //   }
    // },
    // {
    //   type: "line",
    //   mode: "vertical",
    //   scaleID: "x-axis-0",
    //   value: "27.03.",
    //   borderColor: "rgb(75, 192, 192)",
    //   borderWidth: 4,
    //   label: {
    //     content: "Osterferien",
    //     enabled: true,
    //     position: "bottom",
    //     rotation: 90
    //   }
    // },
    // {
    //   type: "line",
    //   mode: "vertical",
    //   scaleID: "x-axis-0",
    //   value: "23.04.",
    //   borderColor: "rgb(75, 192, 192)",
    //   borderWidth: 4,
    //   label: {
    //     content: "IfSG",
    //     enabled: true,
    //     position: "bottom",
    //     rotation: 90
    //   }
    // }
    {
        type: "line",
        mode: "vertical",
        scaleID: "x",
        value: "22.11.",
        borderColor: "rgb(75, 192, 192)",
        borderWidth: 4,
        label: {
          content: "Wellenbrecher",
          enabled: true,
          position: "bottom",
          rotation: 'auto'
        }
      }
  ]
};