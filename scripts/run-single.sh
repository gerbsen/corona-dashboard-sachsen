#!/bin/bash
set -e

POSTGRES="PGUSER=${PGUSER:=corona} PGHOST=${PGHOST:=localhost} PGPASSWORD=${PGPASSWORD} PGDATABASE=${PGDATABASE:=corona} PGPORT=${PGPORT:=5432}"

# download and import the data to the database
# no parameter given means that we crawl from the web
env $POSTGRES node scripts/import/import-sms.js "$@"

# download and import the data to the database
# no parameter given means that we crawl from the web
# env $POSTGRES node scripts/import/import-hospitals-divi.js "$@"

# we add the delta of each day to the day before for convenience
env $POSTGRES node scripts/import/import-insert-delta-day-before.js

# call the other script to not have things duplicated
./scripts/run-generator.sh "$@"