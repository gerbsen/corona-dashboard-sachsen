{
  type: 'bar',
  data: {
    labels: [LABELS],
    datasets: [{
      label: 'GRAPH_DATASET_LABEL',
      backgroundColor: CHART_COLOR,
      borderColor: CHART_COLOR,
      data: [DATA],
      fill: false,
      hidden: false
    }]
  },
  options: {
    responsive: true,
    title: { display: true, text: 'GRAPH_TITLE' },
    tooltips: { mode: 'index', intersect: false },
    hover: { mode: 'nearest', intersect: true },
    legend: { position: 'bottom', display : false },
    scales: {
      xAxis: {
        display: true,
        stacked: true,
        scaleLabel: {
          display: true,
          labelString: 'Kalenderwoche'
        }
      },
      yAxis: {
        display: true,
        stacked: true,
        position: 'right',
        scaleLabel: {
          display: true,
          labelString: 'Y-AXIS_LABEL'
        },
        gridLines: {
          display: true,
          drawBorder: true,
          drawOnChartArea: true,
          drawTicks: true,
        }
      }
    }
  }
}