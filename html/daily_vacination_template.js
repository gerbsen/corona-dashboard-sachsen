{
  type: 'line',  
  data: {
    labels: [LABELS],
    datasets: [{
      label: 'LABEL_TOTAL',
      backgroundColor: window.chartColors.red,
      borderColor: window.chartColors.red,
      data: [DATA_TOTAL],
      fill: false,
      hidden: false
    }
    // ,{
    //   label: 'LABEL_AGE',
    //   backgroundColor: window.chartColors.orange,
    //   borderColor: window.chartColors.orange,
    //   data: [DATA_AGE],
    //   fill: false,
    //   hidden: false
    // },{
    //   label: 'LABEL_JOB',
    //   backgroundColor: window.chartColors.purple,
    //   borderColor: window.chartColors.purple,
    //   data: [DATA_JOB],
    //   fill: false,
    //   hidden: false
    // },{
    //   label: 'LABEL_MED',
    //   backgroundColor: window.chartColors.yellow,
    //   borderColor: window.chartColors.yellow,
    //   data: [DATA_MED],
    //   fill: false,
    //   hidden: false
    // },{
    //   label: 'LABEL_CARE',
    //   backgroundColor: window.chartColors.blue,
    //   borderColor: window.chartColors.blue,
    //   data: [DATA_CARE],
    //   fill: false,
    //   hidden: false
    // }
  ]
  },
  options: {
    responsive: true,
    title: { display: true, text: 'GRAPH_TITLE' },
    tooltips: { mode: 'index', intersect: false },
    hover: { mode: 'nearest', intersect: true },
    legend: { position: 'bottom', display : false },
    scales: {
      xAxis: {
        display: true,
        scaleLabel: {
          display: true,
          labelString: 'X-AXIS_LABEL'
        }
      },
      yAxis: {
        display: true,
        position: 'right',
        scaleLabel: {
          display: true,
          labelString: 'Y-AXIS_LABEL'
        },
        gridLines: {
          display: true,
          drawBorder: true,
          drawOnChartArea: true,
          drawTicks: true,
        }
      }
    }
  }
}