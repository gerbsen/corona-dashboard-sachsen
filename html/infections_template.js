    var diffYesterdayCanvasDIFF_YESTERDAY_IDConfig = {
      type: 'bar',
      data: {
        labels: [DIFF_YESTERDAY_LABELS],
        datasets: [{
          label: 'Neuinfektionen (RKI)',
          backgroundColor: [BACKGROUND_COLORS],
          borderColor: window.chartColors.red,
          data: [DIFF_YESTERDAY_VALUES],
          fill: false,
          order: 1,
          yAxisID: 'y-axis-1'
        },{
          label: 'Neuinfektionen (7-Tages Ø, RKI)',
          backgroundColor: window.chartColors.blue,
          borderColor: window.chartColors.blue,
          data: [DIFF_YESTERDAY_SMOOTHED_VALUES],
          fill: false,
          type: 'line',
          order: 0,
          yAxisID: 'y-axis-1'
        },{
          label: 'Verstorbene (RKI)',
          backgroundColor: "black",
          borderColor: "black",
          data: [DIFF_YESTERDAY_DEAD_VALUES],
          fill: false,
          type: 'line',
          order: 0,
          yAxisID: "y-axis-2"
        } 
          SMS_HOSPITAL_VALUES
        ]        
      },
      options: {
        plugins: {
          annotation: {
            annotations: [
              IMPORTANT_DATES
              NOTBREMSE
            ]
          },
        },
        elements: {
          point:{
            radius: 0
          }
        },
        legend: {
          position: 'bottom'
        },
        interaction: {
          intersect: false,
          mode: 'index',
        },
        responsive: true,
        title: {
          display: true,
          text: 'COVID-19-Fälle/Tag nach Meldedatum'
        },
        tooltips: {
          mode: 'index',
          intersect: false,
        },
        hover: {
          mode: 'nearest',
          intersect: true,
          onHover: function (e, element) {
            console.log(e, element)
            if (element.length !== 0) {
              element = element[0];
              element._chart.data.datasets[0].backgroundColor = new Array(element._chart.data.datasets[0].backgroundColor.length).fill(window.chartColors.red);
              element._chart.data.datasets[0].backgroundColor[element._index + 10] = "#8DE2A3";
              element._chart.data.datasets[0].backgroundColor[element._index + 11] = "#4DD170";
              element._chart.data.datasets[0].backgroundColor[element._index + 12] = "#21823B";
              element._chart.data.datasets[0].backgroundColor[element._index + 13] = "#4DD170";
              element._chart.data.datasets[0].backgroundColor[element._index + 14] = "#8DE2A3";
              element._chart.update();
            }
          }
        },
        scales: {
          x: {
            display: true,
            scaleLabel: {
              display: false,
              labelString: 'Datum'
            }
          },
          "y-axis-1": {
            type: 'linear',
            display: true,
            id: 'y-axis-1',
            position: 'left',
            scaleLabel: {
              display: true,
              labelString: '# Fälle/Tag'
            }
          },
          'y-axis-2' : {
            type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
            display: true,
            position: 'right',
            id: 'y-axis-2',
            scaleLabel: {
              display: false,
              labelString: 'Belegung der Betten auf Normalstation (Sachsen, SMS)'
            },
            // grid line settings
            gridLines: {
              drawOnChartArea: false, // only want the grid lines for one axis to show up
            },
          }
        }
      }
    };
