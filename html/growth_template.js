// const { plugins } = require("chart.js");

var growthGROWTH_KREIS_IDChartConfig = {
  type: 'line',
  data: {
    labels: [GROWTH_LABELS],
    datasets: [
      GROWTH_DATASETS
    ]        
  },
  options: {
    legend: {
      position: 'bottom'
    },
    responsive: true,
    title: {
      display: false,
      text: 'Tägliche Veränderung der 7-Tages Inzidenz im Vergleich zum Vortag'
    },
    tooltips: {
      mode: 'index',
      intersect: false,
    },
    hover: {
      mode: 'nearest',
      intersect: true
    },
    elements: {
      point:{
        radius: 0
      }
    },
    scales: {
      x: {
        display: true,
        scaleLabel: {
          display: false,
          labelString: 'Datum'
        }
      },
      y: {
        type: 'linear',
        display: true,
        position: 'left',
        scaleLabel: {
          display: true,
          labelString: 'Veränderung zum Vortag'
        },
        ticks: {
          callback: function(value){return (value) + "%"}
        },  
        scaleLabel: {
          display: true,
          labelString: "Wachstumsrate (logarithmisch, Basis 2) in [1/Tag]"
        }
      }
    },
    plugins : { 
      annotation: {
        annotations: [
          {
            drawTime: "afterDatasetsDraw",
            id: "hline1",
            type: "line",
            mode: "horizontal",
            scaleID: "y",
            value: (1/30) * 100,
            borderColor: "black",
            borderWidth: 1,
            label: {
              backgroundColor: "#d7191c",
              content: "Wachstum stärker als eine Verdoppelung pro Monat",
              enabled: true
            }
          },
          {
            drawTime: "afterDatasetsDraw",
            id: "hline2",
            type: "line",
            mode: "horizontal",
            scaleID: "y",
            value: (2/30) * 100,
            borderColor: "black",
            borderWidth: 1,
            label: {
              backgroundColor: "#d7191c",
              content: "Wachstum stärker als eine Vervierfachung pro Monat",
              enabled: true
            }
          },
          {
            drawTime: "afterDatasetsDraw",
            id: "hline3",
            type: "line",
            mode: "horizontal",
            scaleID: "y",
            value: (-1/30) * 100,
            borderColor: "black",
            borderWidth: 1,
            label: {
              backgroundColor: "#d7191c",
              content: "Reduktion stärker als eine Halbierung pro Monat",
              enabled: true
            }
          },
          {
            drawTime: "afterDatasetsDraw",
            id: "hline4",
            type: "line",
            mode: "horizontal",
            scaleID: "y",
            value: (-2/30) * 100,
            borderColor: "black",
            borderWidth: 1,
            label: {
              backgroundColor: "#d7191c",
              content: "Reduktion stärker als eine Viertelung pro Monat",
              enabled: true
            }
          }
          IMPORTANT_DATES
        ]
      }
    }
  }
};