{
  type: 'bar',
  data: {
    labels: [DIVI_LABELS],
    datasets: [{
      label: 'DATA_LABEL_1',
      backgroundColor: window.chartColors.green,
      borderColor: window.chartColors.green,
      data: [DATA_1],
      fill: false,
      hidden: false
    },{
      label: 'DATA_LABEL_2',
      backgroundColor: window.chartColors.red,
      borderColor: window.chartColors.red,
      data: [DATA_2],
      fill: false,
      hidden: false
    }]
  },
  options: {
    responsive: true,
    title: { display: true, text: 'DIVI_LABEL' },
    tooltips: { mode: 'index', intersect: false },
    hover: { mode: 'nearest', intersect: true },
    legend: { position: 'bottom' },
    scales: {
      xAxis: {
        display: true,
        stacked: true,
        scaleLabel: {
          display: true,
          labelString: 'Datum'
        }
      },
      yAxis: {
        display: true,
        stacked: true,
        position: 'right',
        scaleLabel: {
          display: true,
          labelString: 'Anzahl der Fälle'
        },
        gridLines: {
          display: true,
          drawBorder: true,
          drawOnChartArea: true,
          drawTicks: true,
        }
      }
    }
  }
}